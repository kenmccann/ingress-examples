# Ingress Testing
----------------

```
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: simple-fanout-example
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
spec:
  rules:
  - host: foo.bar.com
    http:
      paths:
      - path: /foo
        backend:
          serviceName: service1
          servicePort: 4200
      - path: /bar
        backend:
          serviceName: service2
          servicePort: 8080
```

```yaml 
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: test-ingress
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
spec:
  rules:
  - host: kubekenmac.kenmac.io
    http:
      paths:
      - path: /testpath
        backend:
          serviceName: test
          servicePort: 80
```

Examples
- https://github.com/kubernetes/ingress-nginx/issues/4538
- https://kubernetes.io/docs/concepts/services-networking/ingress/
- https://github.com/cw-sakamoto/grpc-example/blob/master/kubernetes/nginx-ingress/nginx-ingress-grpc.yaml
- https://github.com/nginxinc/kubernetes-ingress/tree/master/examples/grpc-services **important** 